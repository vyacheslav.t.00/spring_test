package testing;

import jakarta.transaction.Transactional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import testing.model.Student;
import testing.repo.StudentRepository;


@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = TestingApplication.class
)
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@Transactional
public class StudentTest
{
    @Autowired
    private StudentRepository repository;

    private final Student testStudent = Student.EXAMPLE;


    @BeforeEach
    public void init(){
        repository.deleteAll();
        repository.save(testStudent);
    }

    @Test
    public void testStudent(){
        Assertions.assertThat(testStudent).isNotNull();
        Assertions.assertThat(testStudent.getEmail()).isNotNull();
        Assertions.assertThat(testStudent.getId()).isNotNull();
        Assertions.assertThat(testStudent.getName()).isNotNull();
        Assertions.assertThat(testStudent.getAge()).isNotNull();
    }

    @Test
    public void initTest(){
        Assertions.assertThat(repository.findByEmail(testStudent.getEmail()))
                .isPresent();

    }
@Disabled
    @Test
    public void testRepository(){

    }


}
