package testing.controller;


import testing.exception.DeleteException;
import testing.exception.UpdateException;
import testing.model.Student;
import testing.service.StudentServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentServiceImpl studentService;



    @GetMapping("/get")
    public ResponseEntity<Student> getStudent(@RequestParam String email){
        Optional<Student> student = studentService.getStudentByEmail(email);
        return student.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity
                        .badRequest()
                        .header("X-Error-Message", "No student with such email: " + email)
                        .build()
                );
    }

    @GetMapping("/example")
    public ResponseEntity<Student> getExample(){
        return ResponseEntity.ok(Student.EXAMPLE);
    }

    @PostMapping("/save")
    public ResponseEntity<Void> saveNewStudent(@RequestBody Student student){
        studentService.saveStudent(student);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/update")
    public ResponseEntity<Void> updateStudent(@RequestBody Student student){
        try {
            studentService.updateStudent(student);
            return ResponseEntity.ok().build();
        }catch (UpdateException exception){
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Void> deleteStudent(@RequestParam String email){
        try {
            studentService.deleteStudent(email);
            return ResponseEntity.ok().build();
        }
        catch (DeleteException exception){
            System.err.println("Failed to delete ");
            exception.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }
}
