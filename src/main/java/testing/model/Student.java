package testing.model;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(name = "students")
public class Student {
    public static final Student EXAMPLE
            = new Student(-100L, 20, "example@mail", "Example Example");

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Integer age;
    private String email;
    private String name;




}
