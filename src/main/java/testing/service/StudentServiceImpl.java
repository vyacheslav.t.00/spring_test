package testing.service;

import testing.exception.DeleteException;
import testing.exception.UpdateException;
import testing.model.Student;
import testing.repo.StudentRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService{

    private final StudentRepository repository;


    @Override
    public Optional<Student> getStudentByEmail(String email) {
        return Optional.empty();
    }

    @Override
    public void saveStudent(Student s){
        repository.save(s);
    }

    @Override
    public Optional<Student> findByEmail(String email){
        return repository.findByEmail(email);
    }

    @Override
    @Transactional
    public void updateStudent(Student student) throws UpdateException {
        Optional<Student> optionalStudent = repository.findByEmail(student.getEmail());
        Student s = optionalStudent.orElseThrow(UpdateException::new);
        s.setAge(student.getAge());
        s.setName(student.getName());
    }

    @Override
    public void deleteStudent(String email) throws DeleteException {
        Student student = repository.findByEmail(email).orElseThrow(DeleteException::new);
        repository.delete(student);
    }
}
