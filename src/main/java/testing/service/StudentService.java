package testing.service;

import testing.exception.DeleteException;
import testing.exception.UpdateException;
import testing.model.Student;

import java.util.Optional;

public interface StudentService {

    Optional<Student> getStudentByEmail(String email);

    void saveStudent(Student student);

    Optional<Student> findByEmail(String email);

    void updateStudent(Student student) throws UpdateException;

    void deleteStudent(String email) throws DeleteException;
}
